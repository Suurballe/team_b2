---
title: '20S ITT1 Project Team B2'
subtitle: 'Project plan, part I'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Morten Bo Nielsen \<mon@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: \today
email: 'nisi@ucl.dk'
left-header: \today
right-header: Project plan
skip-toc: false
skip-tof: true
---

# Background

This is the semester project part 1 for ITT1 where we will work with "Proof of concept", end to end communication.


# Purpose

The main goal is to learn about product management, and system development relevant to projects in IT.  
This is a leaning project and the primary objective is for the students to get a good understanding of the challenges involved in Product management/system development and to give them hands-on experience with these tools.
It must be read using the weekly plan as a complementary source for the *learning* oriented goals.


# Goals

Learen how to apply a project management method to development projects in IT, and apply knowledge, methods and tools to the design, development and testing of products or systems.
At the end, the students are able to:
	Manage the interaction between given technologies in integrated solutions
	Carry out planning and quality management of the student�s own technical tasks
	In a structured setting, acquire new knowledge, skills and competencies in specific sub-areas

# Schedule

The project is divided into three phases from week 37 to week 45.

See the [lecture plan](https://eal-itt.gitlab.io/20a-itt1-project/other-docs/20A_ITT1_PROJECT_lecture_plan) for details.


# Organization

Supervisors
Developers
Customers


# Budget and resources

Small monetary resources are expected. In terms of manpower, only the people in the project group are expected to contrbute,


# Risk assessment

The risks involved in the project is evaluated, and the major issues will be listed.  
The plan will include the actions taken as part of the project design to handle the risk or the actions planned should a given risk materialize.

TBD: Needs to be fleshed out.

List of possible failures, pre mortem:
�    Dysfunctional electronics / parts
�    Sickness / Illness
�    Personal issues
�    Too much work / deadline not met
�    Intern Discussions taking over, ruining the ability to work together
�    Misunderstood assignments / tasks
�    Laziness within the group, resulting in parts of the project unfinished

# Stakeholders & Communication

An analysis of the stakeholder is conducted to establish who has an interest in the project. There will be multiple stakeholders with diverse interests in the project.

Possible stakeholders  
	Us:
		Communication: Discord / Element / Messenger / Physical

	Potential customers:
		Communication: Email / Kickstarter

	Teachers:
		Communication: Element / Discord / Email / Physical

	Potential employers:
		Communication: LinkedIn


# Perspectives

This project will serve as a template for other similar projects in future semesters.

It can also serve as an example of the students abilities in their portfolio, when applying for jobs or internships.


# Evaluation

Evaluation is about how to gauge if the project was successful. This includes both the process and the end result. Both of which may have consequences on future projects.  
There will be some documentation needed at the end (or during) the project, e.g. external funding will require some sort of reporting after the project has finished. This must be described.  

When a project is done, we will evaluate our process, work and what we have learned.

# References

Include any relevant references to legal documents, literature, books, home pages or other material that is relevant for the reader.

TBD Students are yet to add their own references

* Common project group on gitlab.com

  The project is managed using gitlab.com. The groups is at [https://gitlab.com/groups/20a-itt1-project](https://gitlab.com/groups/20a-itt1-project)