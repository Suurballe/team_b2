# Team B2
Team B2

Thingspeak project code and recreation guide is found in the "Thingspeak project" folder.  
The main code file is named "Code.py" and is placed in "Thingspeak Project/Code/"  
The main recreation guide is named "Thingspeak_recreation_project.pdf" and is placed in "Thingspeak Project/Documentation"  

Team members, name in format [firstname lastname]
- Aleksis Kairiss
- Alexander Bjørk Andersen
- Dainty Lyka Bihay Olsen
- Daniel Rasmussen
- Jacob Suurballe Petersen
- Nikolaj Hult-Christensen
- Sheriff Dibba