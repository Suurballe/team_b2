Recreation checklist for team b4
===================================================

Team name: b2

Checklist
------------

- [ ] Project plan completed
- [ ] System block diagram completed
- [ ] Recreate documentation clear and understandable
- [ ] ADC communicating with RPi
- [ ] Temperature sensor communicating with RPi
- [ ] BTN/LED board communicating with RPi
- [ ] Data from RPi sent to Thingspeak


Comments
-----------

<add a bulleted list of bug issues you have created on the projects of other team's>
* https://gitlab.com/20a-itt1-project/team_b4/-/issues/27  
<Any comments that may be relevant for you, other team's or teachers. (Could be that something is super successfull)>  
Nothing was done, there was no documentation, and only copied code that did not work.