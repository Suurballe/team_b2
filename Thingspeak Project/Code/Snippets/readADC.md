#### Documentation for readADC snippet

We can convert analog signals to digital, or vice versa, by using an ADC. There are many kinds of ADC's, but here we will be using the [MCP3008 verison ADC with SPI.](https://cdn-shop.adafruit.com/datasheets/MCP3008.pdf)
The MCP3008 operates from 2.7 to 5.5 volts, so it fits perfectly into our system. We will use the MCP3008 to meassure the current flow of voltage, which can be adjusted by using a potentiometer.
Using the first of its 14 channels (channel 0), and GPIO Zero, we can simply write "MCP3008(channel=0)" to get a reading of voltage, convert it to digital, and displaying it on our terminal or wherever we please. 
This information will be stored in a variable we will call *ADC_value*. 

This whole process of reading and converting this information, will be stored and ready to call upon, in a function called "readADC". So when the user requests this information, they can simply call the function and then recieve it:
```
def readADC(ADC_value):
    ADC_value = MCP3008(channel=0)
```