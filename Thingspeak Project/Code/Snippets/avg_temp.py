def readTemp():
    accumulator = 0     # Local variable used for collecting temperature readings
    for _ in range(14)  # We want to go through our loop 14 times. We do not need to do anything with our loop counter, so we can use underscore _ instead of defining a variable to catch that.
        reading = sensor.get_temperature()      # Collect current temperature and save it to a variable
        accumulator += reading  # Adds our temperature reading to our accumulator
        time.sleep(1)   # Waits a second
    avg_temp = accumulator / 14 # Calculates the average of our 14 readings
    response = temperature_channel.update({temperature_field: current_temp})    # Send current temp to thingspeak