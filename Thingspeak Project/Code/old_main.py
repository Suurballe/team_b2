# Using library for thingspeak from https://github.com/mchwalisz/thingspeak
import thingspeak
# time is used to introduce delays in the program
import time
# We want to interface with our raspberry pi
import RPi.GPIO as GPIO
# import LED from GPIOzero to have it easily blink the LED's
from gpiozero import LED
# From the w1thermsensor library we import the W1ThermSensor module
from w1thermsensor import W1ThermSensor
# We import the os to easily get environment variables through the os
import os
# Use dotenv to load environment variables. Better than having a file for every fucking key or ip address etc etc
from dotenv import load_dotenv

# Load environment variables from our .env file into working memory
load_dotenv()

# Set environment variables to global variables (not best practice)
temperature_channel_id = os.getenv('temperature_channel')
# testing_channel_id = os.getenv('testing_channel')
write_key = os.getenv('write_key')
temperature_field = os.getenv('temperature_field')

# Set sensor variable
sensor = W1ThermSensor()

# Set the various buttons and LEDs pins to a variable
# Tell the GPIO module that we'll be using the board layout to determine pins
resetButtonPin = 11
temperatureButtonPin = 13
statusLED = LED(15)         # Please notice that this specific variable is bound to the LED function from GPIOzero
busyLED = 16
GPIO.setmode(GPIO.BOARD)
GPIO.setup(resetButtonPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(temperatureButtonPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

# Set the channel variable up
temperature_channel = thingspeak.Channel(id=temperature_channel_id, api_key=write_key)

# Clean up after ourselves and release any channels the GPIO pins might be reserved for
def destroy():
    GPIO.cleanup()


# Update busyLED based on argument passed to function
def update_busy(new_state):
    if new_state == True:
        GPIO.output(busyLED, GPIO.HIGH)         #Turn on LED
        print("Busy")
    elif new_state == False:
        GPIO.output(busyLED, GPIO.LOW)          #Turn off LED
        print("Idle")


# Update the status LED with a new status in the form of a parameter
def update_status(new_status):
    if new_status == True:                   #if it is true, run the following code
        statusLED.on()                       #set power to high for the first LED if true
    elif new_status == False:                #if it is not, run this section
        statusLED.blink()


# When the reset button is pressed, attempt to connect to thingspeak using an empty
def reset_button():
    try:
        response = temperature_channel.update({})
        if response == 0:       # Response will only be 0 if one has already reached their quota limit
            return False
        else:
            return True
    except:
        print('A connection error has occurred, flashing connection light')
        update_status(False)


# Measure temperature, then return true/false in case of errors
def measureTemperature():
    try:
        print('Measuring!')
        current_temp = sensor.get_temperature()         # Collect current temperature and save it to a variable
        response = temperature_channel.update({temperature_field: current_temp})    # Send current temp to thingspeak
        print(f'It is currently {current_temp} degrees, sent to thingspeak')
        if response == 0:       # Response will only be 0 if one has already reached their quota limit
            return False
        else:
            return True
    except:
        print('A networking error has occured, flashing connection light...')
        update_status(False)


# Function below is a countdown for 15 seconds. It serves as a lockout for the user, so that they won't be able to
# attempt to send data before the thingspeak server is ready again. Not truly needed, but certainly nice to have
def thingspeak_quota_wait():
    secondsToUnlock = 15
    while secondsToUnlock > 0:
        print(f'Channel has been locked, wait {secondsToUnlock} seconds')
        secondsToUnlock -= 1
        time.sleep(1)
    print('Ready!')

def now_ms():
    ms = int(time.time() * 1000)
    return ms

# initialize variables
loop_duration = 15000 # in milliseconds
loop_start = now_ms()

if __name__ == '__main__':

    try:
        while True

if __name__ == '__main__':

    try:
        while True:
            if GPIO.input(resetButtonPin) == GPIO.LOW:
                update_busy(True)               # Turn on the busy LED
                if reset_button():              # if the reset button function returns true
                    print('Connection successful!')
                    update_status(True)         # Turn on the connection LED
                    thingspeak_quota_wait()     # Wait 15 seconds to be able to actually write data again
                    update_busy(False)          # Turn off the busy LED, signalling data can be sent again
                elif not resetButtonPin:
                    # This code will only be reached if a person has sent data, stopped the script and then
                    # attempted to send data again. Its purpose is to simply catch user error
                    print("Connection busy, please wait at least 15 seconds")
            if GPIO.input(temperatureButtonPin) == GPIO.LOW:
                update_busy(True)               # Turn on the busy LED
                if measureTemperature():        # if the temperature function returns true
                    print("Data sent successfully, quota reached, wait for busy light to turn off")
                    thingspeak_quota_wait()     # Wait 15 seconds to be able to actually write data again
                    update_busy(False)          # Turn off the busy LED, signalling data can be sent again
                elif not temperatureButtonPin:
                    # This code will only be reached if a person has sent data, stopped the script and then
                    # attempted to send data again. Its purpose is to simply catch user error
                    print("Connection busy, please wait at least 15 seconds")

    except KeyboardInterrupt:
        destroy()
