Project idea overview
A user likes to play around with nerf and BB guns. They would however like to have statistics like one is able to in modern computer games.
They would like to have a display counter to show them how many bullets/darts are left in the magazine.
They would like to know how many shots has been fired IN TOTAL
Number of reloads
A button to press in case of a misfire. This can be used to calculate "mean time between failures".

Goals
The primary goal of this project will be to create a working model and have it attached to a nerf gun.
Secondary goal (so far) is to either refine current features (such as make it able to differentiate between semi- and fullauto) or add more. 
Tertiary goal would be to have a working model attached to a BB gun.
