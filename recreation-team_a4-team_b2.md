Recreation checklist for team a4
===================================================

Team name: b2

Checklist
------------

- [x] Project plan completed
- [ ] System block diagram completed
- [ ] Recreate documentation clear and understandable
- [x] ADC communicating with RPi
- [x] Temperature sensor communicating with RPi
- [ ] BTN/LED board communicating with RPi
- [x] Data from RPi sent to Thingspeak


Comments
-----------

Bugs
-----------
1. MINIMUM SYSTEM DIAGRAM
- The issue is that the wrong sensor is shown. Temperature model has to be added.
2. WRONG MODULE
- Wrong module (ads7830) is being used in both the code and documentation.
3. GREEN LED
- The green LED has no documentation and it is used instead of the LED we have on the board.
4. NO GUIDE
- No guide on how to set up the board or its components.
5. MISSING DOCUMENTATION
- No documentation as well as missing comments in the code.
6. LIBRARIES
- The issue is that the documentation does not include a guide on downloading the libraries.
7. LIBRARY_FUNCTION
- The issue is that the library "functions" is faulty.
8. LED DOESNT BLINK
- The issue is that the LED 1 does not blink AT ALL!
9. LED2
- The issue is that when the 2nd button is pressed it turns the LED2 on and off, which it is not supposed to.

Comments
-----------
The implementation of the buttons worked flawlessly, something we ourselves had issues with.