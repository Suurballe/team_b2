import RPi.GPIO as GPIO
import time

def update_status(new_status):
    if(new_status==True):                   #if it is true, run the following code
        GPIO.output(led1, GPIO.HIGH)        #set power to high for the first LED if true
    elif(new_status==False):                #if it is not, run this section
        while(new_status==False):           #as long as it is false, keep running this loop
            GPIO.output(led1, GPIO.LOW)     #make led1 blink if it is not true
            time.sleep(0.5)
            GPIO.output(led1, GPIO.HIGH)
            time.sleep(0.5)