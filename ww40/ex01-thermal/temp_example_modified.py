import sys #used for unicode character \u00B0 (percentage symbol)

from w1thermsensor import W1ThermSensor #library for the sensor https://pypi.org/project/w1thermsensor/

# initialize the sensor
sensor = W1ThermSensor()

# Loop 

while True:

    temperature = sensor.get_temperature()
    print(f'Temperature in Celcius: {temperature} degrees')
